package org.iesjoandaustria.animals.control;

import org.iesjoandaustria.animals.model.Model;
import org.iesjoandaustria.animals.vista.Vista;
import org.iesjoandaustria.animals.bd.Bd;
/* Implementa la capa de control de l'aplicació */
public class Control {
    public void run() {
        Vista vista = new Vista();
        Model model = new Model();
        System.out.println( "Hello World!" );
    }
}
